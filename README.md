# Kachur.me

This is the source for my personal website, which is currently hosted on a
DigitalOcean droplet at https://kachur.me. It contains a quick bio, descriptions
of projects I am working and have worked on, links to my work, and my most recent
resume. It is written to be focused on content first and as simple as possible.

Let me know if you have any critiques! Comments and contributions (not that I
expect any) are accepted through the Gitlab UI as you see fit.

--Nicholas Kachur
